import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';



function App({navigation}) {
  return (
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
      <Text> Group List </Text>
      <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 20, marginVertical: 20 }} onPress={() => navigation.navigate('Group')}>
        <Text style={{fontSize:16, color:'blue'}}>Go to Group</Text>  
       </TouchableOpacity> 
      </View>
    );
};


export default App;