import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';



function App({navigation}) {
  return (
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
      <Text> Group </Text>
      <TouchableOpacity style={{ paddingHorizontal: 20, paddingVertical: 20, marginVertical: 20, backgroundColor:'blue' }} onPress={() => navigation.navigate('GroupList')}>
        <Text style={{fontSize:16, color:'white'}}>Go back to group list page</Text>  
       </TouchableOpacity> 
      </View>
    );
};


export default App;