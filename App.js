import React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import GroupList from './pages/GroupList';
import Group from './pages/Group';
const Menu = createStackNavigator();

function App(){
  return (
      <NavigationContainer>
        <Menu.Navigator>
        <Menu.Screen name="GroupList" component={GroupList} />
        <Menu.Screen name="Group" component={Group} />
        </Menu.Navigator>
      </NavigationContainer> 
    );
};


export default App;